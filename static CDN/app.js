var table = $('#example').DataTable( {
    dom: 'Bfrtip', // optional
    select: true,
    buttons: [
        'copy', 'excel', 'pdf',
        'selectAll', 
        'selected', 
        'print',
    ]
} );

if (typeof table.buttons === "function") {
    console.log("buttons: "+ table.buttons().length )
} else {
    console.error("buttons not available")
}

console.log("jQuery version: "+ $.fn.jquery );
console.log( "datatables version: "+ $.fn.dataTable.version );
