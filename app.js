// import $ from "jquery"; // alternative way to load jquery

var $  = require( 'jquery' ); // 

require( 'jszip' );
require( 'pdfmake' ); // issue 4: pdfmake gives fs error on watch/build

require( 'datatables.net-dt' )();
require( 'datatables.net-buttons-dt' )();
require( 'datatables.net-buttons/js/buttons.html5.js' )();
require( 'datatables.net-buttons/js/buttons.print.js' )();
require( 'datatables.net-responsive-dt' )();
require( 'datatables.net-select-dt' )();


var table = $('#example').DataTable( {
    dom: 'Bfrtip', // optional
    select: true,
    buttons: [
        'copy', 'excel', 
        'pdf',
        // issue 3: selectAll, selected or print give Uncaught Unknown button type
        // 'selectAll', 
        // 'selected', 
        // 'print',
    ]
} );

if (typeof table.buttons === "function") {
    console.log("buttons: "+ table.buttons().length )
} else {
    console.error("buttons not available")
}

console.log("jQuery version: "+ $.fn.jquery );
console.log( "datatables version: "+ $.fn.dataTable.version );
