# DataTables and Parcel

## instructions

- install Parcel (globally): `npm install -g parcel-bundler`
- install NPM packages (see below for details): `npm install`
- run with Parcel (watch mode):
  - `parcel index.html`
  - open in browser: `http://localhost:1234/`
- and/or build with Parcel:
  - `parcel build index.html --public-url ./` ("./" is used to create relative paths)
  - open in browser: `index.html` in the `dist` folder

## issues when using Parcel

See post on DataTables forum:  
https://datatables.net/forums/discussion/comment/145306

1. CSS is not loaded
2. no buttons are displayed / loaded
3. selectAll, selected, print, give *Uncaught Unknown button type*
4. pdfmake gives fs error on watch/build

For comparison, these issues do not occur when loading DataTables from CDN. See `./static` folder.

---

### issue 1: CSS is not loaded

Normally, add to `style.css`:

```javascript
@import 'datatables.net-dt';
```

But then Datatables CSS is not loaded.  
Known issue, Allan reported: https://github.com/parcel-bundler/parcel/issues/432

Workaround, add to `style.css`:

```javascript
@import 'datatables.net-dt/css/jquery.datatables.css';
```

---

### issue 2: no buttons are displayed / loaded

None of the buttons are displayed, even though enabled in the DataTables config:

```javascript
var table = $('#example').DataTable( {
    select: true,
    buttons: [
        'copy', 'excel',
    ]
} );
```

They are not only not displayed, they are also not loaded: `table.buttons()` is not available.  
For comparison: `select` does work. Both `select` and `buttons` are loaded using `require`.

---

### issue 3: selectAll, selected, print, give *Uncaught Unknown button type*

In `app.js` add `selected`, `selectAll` or `print`.  
The log shows e.g.:  

```shell
Uncaught Unknown button type: selectAll
```

Chrome dev console shows it is after calling:

http://localhost:1234/node_modules/iconv-lite/encodings/dbcs-data.js (line 108)

```javascript
        encodeAdd: {'€': 0xA2E3},
```

http://localhost:1234/node_modules/unicode-properties/index.js (line 131)

```javascript
  return (ref = exports.getCategory(codePoint)) === 'Zs' || ref === 'Zl' || ref === 'Zp';
```

---

### issue 4: pdfmake gives fs error on watch/build

In `app.js` enable (if not already):

```javascript
require( 'pdfmake' );
```

When running `parcel build index.html`, warnings/errors are shown:

```shell
<path>/node_modules/pdfkit/js/image.js:32:33: Cannot statically evaluate fs argument
<path>/node_modules/png-js/png-node.js:45:29: Cannot statically evaluate fs argument
<path>/node_modules/fontkit/index.js:43:31: Cannot statically evaluate fs argument
<path>/node_modules/pdfkit/js/font/afm.js:11:41: Cannot statically evaluate fs argument
```

These warnings/errors are shown once.  
To show again: `rm -r .cache/ ./dist/`,  
or use `parcel index.html --no-cache`

The messages in detail:

```javascript
⚠️  <path>/node_modules/pdfkit/js/pdfkit.es5.js:2393:41: Cannot statically evaluate fs argument
  2391 |     key: 'open',
  2392 |     value: function open(filename) {
> 2393 |       return new AFMFont(fs.readFileSync(filename, 'utf8'));
       |                                         ^
  2394 |     }
  2395 |   }]);
  2396 |
⚠️  <path>/node_modules/pdfkit/js/pdfkit.es5.js:4325:33: Cannot statically evaluate fs argument
  4323 |           data = new Buffer(match[1], 'base64');
  4324 |         } else {
> 4325 |           data = fs.readFileSync(src);
       |                                 ^
  4326 |           if (!data) {
  4327 |             return;
  4328 |           }
⚠️  <path>/node_modules/png-js/png-node.js:45:29: Cannot statically evaluate fs argument
  43 |     PNG.load = function(path) {
  44 |       var file;
> 45 |       file = fs.readFileSync(path);
     |                             ^
  46 |       return new PNG(file);
  47 |     };
  48 |
⚠️  <path>/node_modules/fontkit/index.js:43:31: Cannot statically evaluate fs argument
  41 |
  42 | fontkit.openSync = function (filename, postscriptName) {
> 43 |   var buffer = fs.readFileSync(filename);
     |                               ^
  44 |   return fontkit.create(buffer, postscriptName);
  45 | };
  46 |
```

---

## detailed instructions on installing DataTables & Parcel

### install DataTables using CDN

Use the Datatables download tool (1. framework: DataTables, 2 packages: jQuery 3, DataTables; extensions: Buttons, HTML5 export, JSZip, pdfmake, Print view, Responsive, Select).

```javascript
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/sl-1.2.6/datatables.min.js"></script>
```

When using download method CDN everything works as expected.

### install DataTables using Parcel

Use DataTables (same selection as for CDN) using the NPM/Yarn instructions:

```shell
npm install --save jquery jszip pdfmake datatables.net-dt datatables.net-buttons-dt datatables.net-responsive-dt datatables.net-select-dt
```

in app.js:

```javascript
var $  = require( 'jquery' );

require( 'jszip' );
require( 'pdfmake' );

require( 'datatables.net-dt' )();
require( 'datatables.net-buttons-dt' )();
require( 'datatables.net-buttons/js/buttons.html5.js' )();
require( 'datatables.net-buttons/js/buttons.print.js' )();
require( 'datatables.net-responsive-dt' )();
require( 'datatables.net-select-dt' )();
```

### install Parcel

Either:

1. **globally**
    - install: `npm install -g parcel-bundler`
    - use: `parcel index.html`
2. **locally**
    - install: `npm install parcel`
    - use: `./node_modules/parcel/bin/cli.js index.html`

